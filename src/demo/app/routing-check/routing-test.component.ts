import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-routing-test',
  template:  `
    <p>Routing test worked</p>
  `
})
export class RoutingTestComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }
}
